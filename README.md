# Sheltertrack Demo
This project is designed to give me an excuse to learn Django and experiment with databases. The end goal is to build a system for tracking the animals in a shelter with a dashboard view that can display animal locations, notes, and medical details.

## Requirements
 - Python 3.8 or newer
 - Django 4.0 or newer

## Sample Data
`db.sqlite3` is checked in to provide some sample data to play with. 