from django.shortcuts import render
from django.views import generic
from django.http import HttpResponse
from django.utils import timezone

from .models import Animal, MedicalNote, Location


class IndexView(generic.ListView):
    template_name = 'kiosk/index.html'
    context_object_name = 'animal_list'

    def get_queryset(self):
        return Animal.objects.filter(intake_date__lte=timezone.now())

class DetailView(generic.DetailView):
    model = Animal
    template_name = 'kiosk/detail.html'

class DashboardView(generic.ListView):
    template_name = 'kiosk/dashboard.html'
    context_object_name = 'dataset'

    def get_queryset(self):
        # There may be a better way to aggregate this information, but this seems to work.
        dataset = {'locations':{}, 'count':0}
        for loc in Location.objects.filter(show_on_dashboard__exact=True):
            dataset['locations'][loc] = {}
            qs = Animal.objects.filter(location__id__exact=loc.id, intake_date__lte=timezone.now(), exit_date__gte=timezone.now()) | Animal.objects.filter(location__id__exact=loc.id, intake_date__lte=timezone.now(), exit_date__isnull=True)
            for animal in qs:
                dataset['locations'][loc][animal] = []
                for note in animal.medicalnote_set.all():
                    if note.is_active():
                        dataset['locations'][loc][animal].append(note)
            # For each location, get a list of active animals in that location
            dataset['count'] += len(dataset['locations'][loc])

        return dataset