from django.db import models
from django.utils import timezone

class Location(models.Model):
    name = models.CharField(max_length=200)
    capacity = models.PositiveIntegerField()
    show_on_dashboard = models.BooleanField(default=True)

    def __str__(self):
        return '{n}'.format(n=self.name)

class Animal(models.Model):
    species = models.CharField(max_length=64)
    name = models.CharField(max_length=200)
    intake_date = models.DateTimeField('intake date')
    exit_date = models.DateTimeField('exit date', default=None, blank=True, null=True)
    location = models.ForeignKey(Location, on_delete=models.PROTECT, null=True, blank=True)

    def __str__(self):
        return '{i}/{s}/{n}'.format(i=self.id, n=self.name, s=self.species)

    @property
    def is_active(self):
        return (self.intake_date <= timezone.now()) and ((self.exit_date is None) or (self.exit_date > timezone.now()))

class MedicalNote(models.Model):
    animal = models.ForeignKey(Animal, on_delete=models.CASCADE)
    start_date = models.DateTimeField('start date')
    end_date = models.DateTimeField('end date')
    instruction = models.CharField(max_length=1024)
    notes = models.CharField(max_length=1024, default=None, blank=True, null=True)

    def __str__(self):
        return '{n} Note {i}'.format(n=self.animal, i=self.id)

    def is_active(self):
        return (self.start_date <= timezone.now()) and ((self.end_date is None) or (self.end_date > timezone.now()))

"""
Other ideas:
 - Split medical and food notes
 - Allow for food, behavior note input
 - Detailed medical notes
  - FIV/FeLV status
  - Microchip Number
  - Fecal results
  - Rabies vaccine info (+ date?)
  - Other vaccine info (name + date?)
"""