from django.contrib import admin

# Register your models here.
from .models import Animal, MedicalNote, Location
admin.site.register(Animal)
admin.site.register(MedicalNote)
admin.site.register(Location)
